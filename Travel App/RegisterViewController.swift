//
//  RegisterViewController.swift
//  Travel App
//
//  Created by Alek Ivanovski on 6/26/19.
//  Copyright © 2019 Alek Ivanovski. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController {

    @IBOutlet weak var emailTextView: UITextField!
    @IBOutlet weak var passwordTextView: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func registerPressed(_ sender: Any) {
        Auth.auth().createUser(withEmail: emailTextView.text!, password: passwordTextView.text!) { (user, error) in
            if error != nil {
                print(error)
            } else {
//                success
                print("Registration successful")
                self.performSegue(withIdentifier: "mainMenuSegue2", sender: self)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
