//
//  UserProfileViewController.swift
//  Travel App
//
//  Created by Alek Ivanovski on 6/26/19.
//  Copyright © 2019 Alek Ivanovski. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameTextView: UILabel!
    @IBOutlet weak var enterNameTextField: UITextField!
    
    let picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let name = UserDefaults.standard.string(forKey: "username") {
            nameTextView.text = name
        }
        picker.delegate = self
    }
    
    @IBAction func changeNamePressed(_ sender: Any) {
        if let newName = enterNameTextField.text {
            UserDefaults.standard.set(newName, forKey: "username")
            nameTextView.text = newName
        }
    }
    
    @IBAction func importImage(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            noCamera()
        }
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        profileImage.contentMode = .scaleAspectFit
        profileImage.image = chosenImage
        dismiss(animated: true, completion: nil)
    }

}
