//
//  AccelerometerViewController.swift
//  Travel App
//
//  Created by Alek Ivanovski on 6/27/19.
//  Copyright © 2019 Alek Ivanovski. All rights reserved.
//

import UIKit
import CoreMotion

class AccelerometerViewController: UIViewController {
    @IBOutlet weak var xTextView: UITextField!
    @IBOutlet weak var yTextView: UITextField!
    @IBOutlet weak var zTextView: UITextField!
    
    var motion = CMMotionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        accelerometer()
    }
    
    func accelerometer() {
        motion.accelerometerUpdateInterval = 0.5
        motion.startAccelerometerUpdates(to: OperationQueue.current!) { (data, error) in
            print(data as Any)
            if let trueData = data {
                self.view.reloadInputViews()
                let x = trueData.acceleration.x
                let y = trueData.acceleration.y
                let z = trueData.acceleration.z
                self.xTextView.text = "x: \(Double(x).rounded(toPlaces: 3 ))"
                self.yTextView.text = "y: \(Double(y).rounded(toPlaces: 3 ))"
                self.zTextView.text = "z: \(Double(z).rounded(toPlaces: 3 ))"
            }
        }
    }
    
}

extension Double {
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded()
    }
}
