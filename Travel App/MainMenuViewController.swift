//
//  MainMenuViewController.swift
//  Travel App
//
//  Created by Alek Ivanovski on 6/26/19.
//  Copyright © 2019 Alek Ivanovski. All rights reserved.
//

import UIKit

class MainMenuViewController: UIViewController {

    @IBOutlet weak var locationButton: CustomButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
    }
    
    @IBAction func locationPressed(_ sender: Any) {
        locationButton.shakeButton()
        performSegue(withIdentifier: "mapSegue", sender: self)
    }
    
    @IBAction func profilePressed(_ sender: Any) {
        performSegue(withIdentifier: "profileSegue", sender: self)
    }
    
    @IBAction func accelerometerPressed(_ sender: Any) {
        performSegue(withIdentifier: "sensorSegue", sender: self)
    }
}
