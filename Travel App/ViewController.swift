//
//  ViewController.swift
//  Travel App
//
//  Created by Alek Ivanovski on 6/25/19.
//  Copyright © 2019 Alek Ivanovski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func loginPressed(_ sender: Any) {
        performSegue(withIdentifier: "loginSegue", sender: self)
    }
    
    @IBAction func registerPressed(_ sender: Any) {
        performSegue(withIdentifier: "registerSegue", sender: self)
    }
}

