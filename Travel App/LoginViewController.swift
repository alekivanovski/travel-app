//
//  LoginViewController.swift
//  Travel App
//
//  Created by Alek Ivanovski on 6/26/19.
//  Copyright © 2019 Alek Ivanovski. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextView: UITextField!
    @IBOutlet weak var passwordTextView: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailTextView.text!, password: passwordTextView.text!) { (user, error) in
            if error != nil {
                print(error)
            } else {
                self.performSegue(withIdentifier: "mainMenuSegue", sender: self)
            }
        }
    }

}
